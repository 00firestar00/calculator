package com.evan.calculator;


import org.javia.arity.Symbols;
import org.javia.arity.SyntaxException;

public class Logic {
   private static Symbols symbols = new Symbols();
   
   public static boolean isOperator( char c ) {
      return "+\u2212\u00d7\u00f7/*".indexOf(c) != -1;
   }
   
   public static String evaluate(String input) throws SyntaxException {
      if (input.trim().equals("")) {
          return "";
      }

      // drop final infix operators (they can only result in error)
      int size = input.length();
      while (size > 0 && isOperator(input.charAt(size - 1))) {
          input = input.substring(0, size - 1);
          --size;
      }
      return Double.toString(symbols.eval(input));

//      String result = "";
//      for (int precision = mLineLength; precision > 6; precision--) {
//          result = tryFormattingWithPrecision(value, precision);
//          if (result.length() <= mLineLength) {
//              break;
//          }
//      }
//      return result.replace('-', MINUS).replace(INFINITY, INFINITY_UNICODE);
  }
}
