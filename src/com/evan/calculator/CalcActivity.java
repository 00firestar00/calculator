package com.evan.calculator;

import org.javia.arity.SyntaxException;

import android.app.Activity;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Handles the main activity
 * 
 * @author Evan
 */
public class CalcActivity extends Activity implements OnClickListener, OnLongClickListener {

   protected EditText text_input;
   private Boolean reset = false;

   @Override
   protected void onCreate( Bundle savedInstanceState ) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_calc);

      initControls();
   }

   protected void onPause( ) {
      super.onPause();
   }

   protected void onResume( ) {
      super.onResume();
   }

   @Override
   public boolean onCreateOptionsMenu( Menu menu ) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.activity_calc, menu);
      return true;
   }

   //   @Override
   //   public boolean onOptionsItemSelected( MenuItem item ) {
   //       int id = item.getItemId();
   //       if (id == R.id.Settings) {
   //           startActivity(new Intent(this, com.comp350.calculator.CalcSettings.class));
   //           return true;
   //       }
   //       else {
   //           return super.onOptionsItemSelected(item);
   //       }
   //   }

   private void initControls( ) {

      text_input = ( EditText ) findViewById(R.id.TextArea);
      text_input.setOnClickListener(this);

      final Resources res = getResources();
      final TypedArray button_array = res.obtainTypedArray(R.array.buttons);
      for (int i = 0; i < button_array.length(); i++) {
         (( Button ) findViewById(button_array.getResourceId(i, 0))).setOnClickListener(this);
      }
      button_array.recycle();

      (( ImageButton ) findViewById(R.id.DelButton)).setOnClickListener(this);
      (( ImageButton ) findViewById(R.id.DelButton)).setOnLongClickListener(this);

   }

   @Override
   public void onClick( View v ) {

      int id = v.getId();
      switch (id) {
      case R.id.DelButton:
         onDelete();
      break;

      case R.id.EqualButton:
         onEnter();
         reset = true;
      break;

      default:
         if (v instanceof Button) {
            String text = (( Button ) v).getText().toString();
            if (text.length() >= 2) {
               // add paren after sin, cos, ln, etc. from buttons
               text += '(';
            }
            insert(text);
         }
      }
   }

   @Override
   public boolean onLongClick( View v ) {
      text_input.setText("");
      return true;
   }

   private void onDelete( ) {
      int length = text_input.getText().length();
      
      if (reset) {
         text_input.setText("");
         reset = false;
      }
      else if (length != 0) {
         text_input.getText().delete(length - 1, length);
      }
   }

   private void onEnter( ) {
      try {
         text_input.setText(Logic.evaluate(text_input.getText().toString()));
      }
      catch (SyntaxException e) {
         text_input.setText("ERROR");
      }
   }

   private void insert( String text ) {
      int length = text_input.getText().length();

      if (Logic.isOperator(text.charAt(0)) && Logic.isOperator(getLastChar()))
         text_input.getText().replace(length - 1, length, text);
      else
         text_input.append(text);
   }

   private char getLastChar( ) {
      int length = text_input.getText().length();
      if (length != 0)
         return text_input.getText().charAt(length - 1);
      else
         return '\0';

   }
}
